<?php

/**
 * @file
 * Template file for nested publication articles node reference inserted via the Insert module.
 *
 * Available variables:
 * - $item: The complete item being inserted.
 * - $api_call: RESTFUL API link to nested articles resource.
 *
 * Modules may provide placeholders that will be replaced by user-entered values
 * when the item is inserted into a textarea. Generic links only support one
 * placeholder.
 *
 * Available placeholders:
 * - __nid__: The node id.
 */
?>
<div class="uwm-nested-insert-wrap row">
  <div data-reveal-id="nestedArticle-{{ nestedArticles[0].id }}" tabindex="0" role="button" class="open-nestedArticle uwm-nested-insert small-11 small-centered  medium-8 columns">
    <div class="uwm-nested-insert-info">
      <div class="uwm-top-white"></div>
      <div class="uwm-nested-insert-meta">Article Sidebar</div>
      <h2 class="uwm-nested-insert-headline">{{ nestedArticles[0].headline }}</h2>
      <p>{{ nestedArticles[0].teaser }}</p>
      <span class="button">Read More</span>
      <div class="uwm-bottom-white"></div>
    </div>
  </div>
</div>
