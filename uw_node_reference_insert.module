<?php

/**
 * @file
 * Allows insertion of node reference directly into the body field.
 *
 * Uses an "Insert" button next to the node reference autocomplete.
 */

/**
 * Implements hook_element_info().
 */
function uw_node_reference_insert_element_info() {
  $extra = array('#after_build' => array('uw_node_reference_insert_element_process'));
  $elements = array();
  $element_type = 'textfield';
  $elements[$element_type] = $extra;
  return $elements;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function uw_node_reference_insert_form_publication_article_node_form_alter(&$form, &$form_state, $form_id) {
  $field = 'field_nested_articles';
  $langcode = $form[$field]['#language'];

  $index = 0;
  while (isset($form[$field][$langcode][$index])) {
    $insert_button = array(
      '#type' => 'submit',
      '#value' => 'Insert',
      '#weight' => '2',
      '#attributes' => array(
        'class' => array(
          'insert-node-reference-button',
        ),
        'rel' => array(
          'node_reference_autocomplete',
        ),
        'onclick' => array(
          'return false;',
        ),
      ),
      '#states' => array(
        'visible' => array(
          ':input[name="field_nested_articles[' . $langcode . '][' . $index . '][nid]"]' => array('filled' => TRUE),
        ),
      ),
    );
    $insert_template = array(
      '#type' => 'hidden',
      '#value' => theme('insert_node_reference_autocomplete', array(), array()),
      '#attributes' => array(
        'class' => array(
          'insert-template',
        ),
      ),
    );
    $form[$field][$langcode][$index]['insert'] = $insert_button;
    $form[$field][$langcode][$index]['insert_template'] = $insert_template;
    $index++;
  }

}

/**
 * Implements hook_insert_widgets().
 */
function uw_node_reference_insert_insert_node_reference_widgets() {
  return array(
    'node_reference_autocomplete' => array(
      'element_type' => 'textfield',
      'wrapper' => 'td',
      'fields' => array(
        'nid' => 'input[name$="[nid]"], textfield[name$="[nid]"]',
      ),
    ),
  );
}

/**
 * Get a list of all supported field widgets.
 */
function uw_node_reference_insert_node_reference_widgets($reset = FALSE) {
  static $widgets;

  if (!isset($widgets) || $reset) {
    $widgets = array();
    foreach (module_implements('insert_node_reference_widgets') as $module) {
      $module_widgets = module_invoke($module, 'insert_node_reference_widgets');
      foreach ($module_widgets as $type => $widget) {
        $module_widgets[$type]['type'] = $type;
        $module_widgets[$type]['module'] = $module;
      }
      $widgets = array_merge($widgets, $module_widgets);
    }
    drupal_alter('insert_node_reference_widgets', $widgets);
  }

  return $widgets;
}

/**
 * Load a single insert node reference field widget info.
 */
function uw_node_reference_insert_widget_load($widget_type) {
  $widgets = uw_node_reference_insert_node_reference_widgets();
  return isset($widgets[$widget_type]) ? $widgets[$widget_type] : FALSE;
}

/**
 * Implements hook_theme().
 */
function uw_node_reference_insert_theme() {
  return array(
    'insert_node_reference_autocomplete' => array(
      'variables' => array('item' => NULL, 'widget' => NULL),
      'template' => 'templates/insert-node-reference-autocomplete',
    ),
  );
}

/**
 * Process function for insert-enabled fields.
 */
function uw_node_reference_insert_element_process($element) {
  static $js_added;
  // Bail out early if the needed properties aren't available. This happens
  // most frequently when editing a field configuration.
  if (!isset($element['#entity_type'])) {
    return $element;
  }

  $item = $element['#value'];
  $field = field_info_field($element['#field_name']);
  $instance = field_info_instance($element['#entity_type'], $element['#field_name'], $element['#bundle']);

  $widget_settings = $instance['widget']['settings'];
  $widget_type = $instance['widget']['type'];

  // Bail out of Insert is not enabled on this field.
  if (empty($widget_settings['insert'])) {
    return $element;
  }

  // Add base settings only once.
  if (!isset($js_added)) {
    $js_added = array();
    drupal_add_js(drupal_get_path('module', 'uw_node_reference_insert') . '/insert-node-reference.js');
  }
  // Add settings for this widget only once.
  if (!isset($js_added[$widget_type])) {
    $js_added[$widget_type] = TRUE;
    $insert_widget = uw_node_reference_insert_widget_load($widget_type);
    $insert_settings = array(
      'wrapper' => $insert_widget['wrapper'],
      'fields' => $insert_widget['fields'],
    );
    drupal_add_js(array('insert' => array('widgets' => array($widget_type => $insert_settings))), 'setting');
  }
  return $element;
}

/**
 * Implements hook_field_widget_info_alter().
 *
 * A list of settings needed by Insert node reference module on widgets.
 */
function uw_node_reference_insert_field_widget_info_alter(&$info) {
  $settings = array(
    'insert' => 0,
    'insert_class' => '',
  );
  // Add a setting to all field types.
  foreach (uw_node_reference_insert_insert_node_reference_widgets() as $widget_type => $widget) {
    if (isset($info[$widget_type]['settings'])) {
      $info[$widget_type]['settings'] += $settings;
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function uw_node_reference_insert_form_field_ui_field_edit_form_alter(&$form, $form_state) {
  $instance = $form['#instance'];
  if (isset($instance['widget']['type'])) {
    $widget = $instance['widget']['type'];
    if ($widget == 'node_reference_autocomplete') {

      $field = $form['#field'];
      if (empty($form['instance']['settings'])) {
        $form['instance']['settings'] = array();
      }
      $form['instance']['settings'] += uw_node_reference_insert_field_widget_settings_form($field, $instance);
    }
  }
}

/**
 * Configuration form for editing insert settings for a field instance.
 */
function uw_node_reference_insert_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  $form['insert'] = array(
    '#type' => 'fieldset',
    '#title' => t('Insert'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('These options allow the user to easily insert an HTML tags into text areas or WYSIWYG editors after uploading a file or image. The "Automatic" style will insert a &lt;img&gt; tag for images and a &lt;a&gt; tag for other files. Other styles may insert tags that may not match the file type.'),
    '#weight' => 20,
    '#parents' => array('instance', 'widget', 'settings'),
  );

  $form['insert']['insert'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable insert button'),
    '#default_value' => $settings['insert'],
    '#description' => t('Enable the insert button and options for this widget.'),
    '#weight' => -10,
  );

  $form['insert']['insert_class'] = array(
    '#title' => t('Additional CSS classes'),
    '#type' => 'textfield',
    '#default_value' => $settings['insert_class'],
    '#description' => t('Add any classes that should be added to the item on output.'),
    '#weight' => 5,
  );

  return $form;
}

/**
 * Preprocess variables for the insert-widget.tpl.php file.
 */
function template_preprocess_insert_node_reference_autocomplete(&$vars) {
}

/**
 * Implements hook_adjust_ckeditor().
 */
function uw_node_reference_insert_adjust_ckeditor() {
  return array(
    'uw_tf_standard' => array(
      'css_path' => base_path() . drupal_get_path('module', 'uw_node_reference_insert') . '/css/ckeditor.css',
    ),
  );
}
